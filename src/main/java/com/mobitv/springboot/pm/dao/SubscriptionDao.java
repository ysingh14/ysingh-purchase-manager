package com.mobitv.springboot.pm.dao;

import com.mobitv.springboot.pm.entity.Subscription;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

public interface SubscriptionDao extends CrudRepository<Subscription, Long> {

}
