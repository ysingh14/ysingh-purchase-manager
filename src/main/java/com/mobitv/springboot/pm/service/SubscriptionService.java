package com.mobitv.springboot.pm.service;

import com.mobitv.springboot.pm.dao.SubscriptionDao;
import com.mobitv.springboot.pm.entity.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class SubscriptionService {

	@Autowired
	private SubscriptionDao subscriptionDao;

	public void addSubscription(Subscription subscription){
		subscriptionDao.save(subscription);
	}

	public Subscription findById(Long subscriptionId){
		return subscriptionDao.findOne(subscriptionId);
	}
}
