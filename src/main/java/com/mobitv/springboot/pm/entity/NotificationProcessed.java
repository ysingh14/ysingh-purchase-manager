package com.mobitv.springboot.pm.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "NOTIFICATION_PROCESSED", uniqueConstraints = { @UniqueConstraint(columnNames = { "notification_identifier" }) })
public class NotificationProcessed implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long notificationProcessedId;
	private String notificationIdentifier;
	private Date timeProcessed;

	public NotificationProcessed() {
	}

	public NotificationProcessed(String notificationIdentifier,
			Date timeProcessed) {
		this.notificationIdentifier = notificationIdentifier;
		this.timeProcessed = timeProcessed;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "notificationProcessedIdGen")
	@SequenceGenerator(name = "notificationProcessedIdGen", sequenceName = "NOTIFICATION_PROCESSED_SEQ")
	@Column(name = "NOTIFICATION_PROCESSED_ID", nullable = false, precision = 22, scale = 0)
	public Long getNotificationProcessedId() {
		return notificationProcessedId;
	}

	public void setNotificationProcessedId(Long notificationProcessedId) {
		this.notificationProcessedId = notificationProcessedId;
	}

	@Column(name = "NOTIFICATION_IDENTIFIER", length = 256)
	public String getNotificationIdentifier() {
		return this.notificationIdentifier;
	}

	public void setNotificationIdentifier(String notificationIdentifier) {
		this.notificationIdentifier = notificationIdentifier;
	}

	@Column(name = "TIME_PROCESSED", nullable = false)
	public Date getTimeProcessed() {
		return this.timeProcessed;
	}

	public void setTimeProcessed(Date timeProcessed) {
		this.timeProcessed = timeProcessed;
	}

	@Override
	public String toString() {
		return "NotificationProcessed [notificationProcessedId="
				+ notificationProcessedId + ", notificationIdentifier="
				+ notificationIdentifier + ", timeProcessed=" + timeProcessed
				+ "]";
	}

}
