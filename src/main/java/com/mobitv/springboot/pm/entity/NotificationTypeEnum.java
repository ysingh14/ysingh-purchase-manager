package com.mobitv.springboot.pm.entity;

public enum NotificationTypeEnum {

	create("create:EN"),

	confirm("confirm:EN"),

	cancel("cancel:EN"),

	renew("renew:EN"),

	renew_failed("renew_failed:EN"),

	do_not_renew("do_not_renew:EN"),

	update_user("update_user:EN"),
	
	start_trial("start_trial:EN"),

	cancel_trial("cancel_trial:EN"),

	;
	
	private final String label;
	
	NotificationTypeEnum(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
	public String toString() {
		return getLabel();
	}
	
}
