package com.mobitv.springboot.pm.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.codehaus.jackson.annotate.JsonAutoDetect;
import org.codehaus.jackson.annotate.JsonIgnore;

@Data
@NoArgsConstructor
@AllArgsConstructor
@XmlRootElement
@Builder
@Entity
@Table(name = "SUBSCRIPTION")
public class Subscription {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "subscriptionIdGen")
	@SequenceGenerator(name="subscriptionIdGen", sequenceName="SUBSCRIPTION_SEQ")
	@Column(name = "ID", nullable = false, precision = 22, scale = 0)
	private Long subscriptionId;

	@Embedded
	private TrackerEntities trackerEntities;

	@Column(name = "ACCOUNT_ID", nullable = false, length = 256)
	private String accountId;

	@Column(name = "VUID", nullable = true, length = 100)
	private String vuid;

	@Column(name = "ALTERNATE_VUID", nullable = true, length = 100)
	private String alternateVuid;

	@Column(name = "VERSION", nullable = false, length = 10)
	private String version;

	@Column(name = "DEVICE_NAME", nullable = true, length = 256)
	private String deviceName;

	@Column(name = "OFFER_ID", nullable = false, length = 256)
	private String offerId;

	@Column(name = "VENDOR_OFFER_ID")
	private String vendorOfferId;

	@Column(name = "TRIAL_VENDOR_OFFER_ID")
	private String trialVendorOfferId;

	@Column(name = "CARRIER", nullable = false, length = 20)
	private String carrier;

	@Column(name = "PRODUCT", nullable = false, length = 40)
	private String product;

	@Column(name = "SUBSCRIBED_DATE", nullable = false)
	private Date subscribedDate;

	@Column(name = "START_DATE")
	private Date startDate;

	@Column(name = "EXPIRES_DATE")
	private Date expiresDate;

	@Column(name = "RENEW_DATE")
	private Date renewDate;

	@Column(name = "FREE_TRIAL_END_DATE")
	private Date freeTrialEndDate;

	@Column(name = "CARRIER_START_DATE", nullable = true)
	private Date carrierStartDate;

	@Column(name = "CARRIER_EXPIRES_DATE", nullable = true)
	private Date carrierExpiresDate;

	@Column(name = "CARRIER_RENEW_DATE", nullable = true)
	private Date carrierRenewDate;

	@Column(name = "LAST_MRC_DATE", nullable = true)
	private Date lastMrcDate;

	@Column(name = "VENDOR_CAMPAIGN_ID", length = 40)
	private String vendorCampaignId;

	@Column(name = "TRIAL_VENDOR_CAMPAIGN_ID", length = 40)
	private String trialVendorCampaignId;

	@Column(name = "VENDOR_PURCHASE_ACTIVITY_ID")
	private String vendorPurchaseActivityId;

	@Column(name = "VENDOR_PURCHASE_INST_ID")
	private String vendorPurchaseInstId;

	@Column(name = "VENDOR", length = 50)
	private String vendor;

	@Column(name = "PURCHASE_MODEL", length = 256)
	private String purchaseModel;

	@Column(name = "SUBSCRIPTION_SOURCE", nullable = false, length = 50)
	private String subscriptionSource;

	@Column(name = "PREVIOUS_SUBSCRIPTION_ID", precision = 22, scale = 0)
	private Long previousSubscriptionId;

	@Column(name = "CANCELLATION_SOURCE", length = 50)
	private String cancellationSource;

	@Column(name = "CANCELLATION_DATE")
	private Date cancellationDate;

	@Column(name = "SUBSCRIPTION_ORIGIN", nullable = true, length = 128)
	private String subscriptionOrigin;

	@Column(name = "PURCHASE_OFFER_TYPE", length = 30)
	private String purchaseOfferType;

	@Column(name = "PURCHASED_BY", length = 100)
	private String purchasedBy;

	@Column(name = "TRIAL_START_DATE")
	private Date trialStartDate;

	@JsonIgnore
	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subscription")
	private Set<BillingHistory> billingHistories = new HashSet<BillingHistory>(0);

	@JsonIgnore
	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subscription")
	private Set<PendingTransaction> pendingTransactions = new HashSet<PendingTransaction>(0);

	@JsonIgnore
	@XmlTransient
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "subscription")
	private Set<PromotionUsed> promotionUsed = new HashSet<PromotionUsed>(0);

}
