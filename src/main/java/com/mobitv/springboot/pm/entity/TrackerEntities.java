package com.mobitv.springboot.pm.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class TrackerEntities {

	@Column(name = "CREATED_TIME", nullable = false)
	private Date createdTime;
	@Column(name = "CREATED_BY", nullable = false, length = 30)
	private String createdBy;
	@Column(name = "LAST_UPDATED_TIME", nullable = false)
	private Date lastUpdatedTime;
	@Column(name = "LAST_UPDATED_BY", nullable = false, length = 30)
	private String lastUpdatedBy;
}
