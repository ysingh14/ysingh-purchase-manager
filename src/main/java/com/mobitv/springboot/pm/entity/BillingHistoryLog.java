package com.mobitv.springboot.pm.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "BILLING_HISTORY")
public class BillingHistoryLog implements Serializable{

	private static final long serialVersionUID = 1L;
	private Long billingHistoryId;
	private Long subscriptionId;
	private Date transactionDate;
	private String carrier;
	private String offerId;
	private String activityType;
	private String activityIdentifier;
	private String vendorRenewalActivityId;
	private BigDecimal amount;
	private Date renewDate;
	private Date newRenewDate;
	private String status;
	private String reasonCode;
	private String notes;
	private Date createdTime;
	private Date lastUpdatedTime;
	private String createdBy;
	private String lastUpdatedBy;

	public BillingHistoryLog() {
	}

	public BillingHistoryLog(Long billingHistoryId,
			Long subscriptionId, Date transactionDate,
			Date createdTime, Date lastUpdatedTime,
			String createdBy, String lastUpdatedBy) {
		this.billingHistoryId = billingHistoryId;
		this.subscriptionId = subscriptionId;
		this.transactionDate = transactionDate;
		this.createdTime = createdTime;
		this.lastUpdatedTime = lastUpdatedTime;
		this.createdBy = createdBy;
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public BillingHistoryLog(Long billingHistoryId,
			Long subscriptionId, Date transactionDate,
			BigDecimal amount, Date renewDate,
			Date newRenewDate, String status, String reasonCode,
			String notes, Date createdTime,
			Date lastUpdatedTime, String createdBy, String lastUpdatedBy) {
		this.billingHistoryId = billingHistoryId;
		this.subscriptionId = subscriptionId;
		this.transactionDate = transactionDate;
		this.amount = amount;
		this.renewDate = renewDate;
		this.newRenewDate = newRenewDate;
		this.status = status;
		this.reasonCode = reasonCode;
		this.notes = notes;
		this.createdTime = createdTime;
		this.lastUpdatedTime = lastUpdatedTime;
		this.createdBy = createdBy;
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO, generator = "billingHistoryIdGen")
	@SequenceGenerator(name="billingHistoryIdGen", sequenceName="BILLING_HISTORY_SEQ")
	@Column(name = "BILLING_HISTORY_ID", nullable = false, precision = 22, scale = 0)
	public Long getBillingHistoryId() {
		return this.billingHistoryId;
	}

	public void setBillingHistoryId(Long billingHistoryId) {
		this.billingHistoryId = billingHistoryId;
	}

	
	@Column(name = "SUBSCRIPTION_ID", nullable = false)
	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	@Column(name = "TRANSACTION_DATE", nullable = false)
	public Date getTransactionDate() {
		return this.transactionDate;
	}

	public void setTransactionDate(Date transactionDate) {
		this.transactionDate = transactionDate;
	}

	@Column(name="CARRIER", length=20)
	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	@Column(name="OFFER_ID", length=255)
	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}

	@Column(name="ACTIVITY_TYPE", length=30)
	public String getActivityType() {
		return activityType;
	}

	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	@Column(name="ACTIVITY_IDENTIFIER", length=30)
	public String getActivityIdentifier() {
		return activityIdentifier;
	}

	public void setActivityIdentifier(String activityIdentifier) {
		this.activityIdentifier = activityIdentifier;
	}

	@Column(name="VENDOR_RENEWAL_ACTIVITY_ID", length=100)
	public String getVendorRenewalActivityId() {
		return vendorRenewalActivityId;
	}

	public void setVendorRenewalActivityId(String vendorRenewalActivityId) {
		this.vendorRenewalActivityId = vendorRenewalActivityId;
	}

	@Column(name = "AMOUNT", precision = 22, scale=2)
	public BigDecimal getAmount() {
		return this.amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Column(name = "RENEW_DATE")
	public Date getRenewDate() {
		return this.renewDate;
	}

	public void setRenewDate(Date renewDate) {
		this.renewDate = renewDate;
	}

	@Column(name = "NEW_RENEW_DATE")
	public Date getNewRenewDate() {
		return this.newRenewDate;
	}

	public void setNewRenewDate(Date newRenewDate) {
		this.newRenewDate = newRenewDate;
	}

	@Column(name = "STATUS", length = 30)
	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "REASON_CODE", length = 100)
	public String getReasonCode() {
		return this.reasonCode;
	}

	public void setReasonCode(String reasonCode) {
		this.reasonCode = reasonCode;
	}

	@Column(name = "NOTES", length = 1000)
	public String getNotes() {
		return this.notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	@Column(name = "CREATED_TIME", nullable = false)
	public Date getCreatedTime() {
		return this.createdTime;
	}

	public void setCreatedTime(Date createdTime) {
		this.createdTime = createdTime;
	}

	@Column(name = "LAST_UPDATED_TIME", nullable = false)
	public Date getLastUpdatedTime() {
		return this.lastUpdatedTime;
	}

	public void setLastUpdatedTime(Date lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	@Column(name = "CREATED_BY", nullable = false, length = 30)
	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "LAST_UPDATED_BY", nullable = false, length = 30)
	public String getLastUpdatedBy() {
		return this.lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	@Override
	public String toString() {
		return "BillingHistory [billingHistoryId=" + billingHistoryId
				+ ", transactionDate=" + transactionDate + ", carrier="
				+ carrier + ", offerId=" + offerId + ", activityType="
				+ activityType + ", activityIdentifier=" + activityIdentifier
				+ ", vendorRenewalActivityId=" + vendorRenewalActivityId
				+ ", amount=" + amount + ", renewDate=" + renewDate
				+ ", newRenewDate=" + newRenewDate + ", status=" + status
				+ ", reasonCode=" + reasonCode + ", notes=" + notes
				+ ", createdTime=" + createdTime + ", lastUpdatedTime="
				+ lastUpdatedTime + ", createdBy=" + createdBy
				+ ", lastUpdatedBy=" + lastUpdatedBy + "]";
	}
	
}
