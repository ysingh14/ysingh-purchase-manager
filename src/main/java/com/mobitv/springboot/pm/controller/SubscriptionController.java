package com.mobitv.springboot.pm.controller;

import com.mobitv.springboot.pm.entity.Subscription;
import com.mobitv.springboot.pm.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class SubscriptionController {

	@Autowired
	private SubscriptionService subscriptionService;

	@RequestMapping("/subscription/{id}")
	public Subscription getSubscriptionById(@PathVariable Long id){
		return subscriptionService.findById(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/subscription")
	public void addSubscription(@RequestBody Subscription subscription){
		subscriptionService.addSubscription(subscription);
	}

	@RequestMapping("/actuator/health")
	public String healthCheck(){
		return "Good Health";
	}

}
