#!/usr/bin/env groovy

node('sonar') {
    def BuildStages = ['Checkout Repository',
                       'Packaging',
                       'Update RC Version',
                       'Uploading RPM to mobi-snapshot repo',
                       'Archive and publishing Junit results',
                       'Publishing Code coverage on jenkins',
                       'Sending Results to Sonarqube',
                       'Deploying to CI-CP'
    ]
    def currentBuildStage
    def repoOwner = 'mobitv'
    def repoContainer = 'git@bitbucket.org'
    def repoName = scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
    def automationRepoName = ''
    def repoUrl = "${repoContainer}:${repoOwner}/${repoName}"
    def buildBranch = env.BRANCH_NAME
    def isMasterOrReleaseBranch = buildBranch == 'master' || buildBranch.startsWith('rel')

    def gitCredential = '6ed9601c-884d-492f-b38f-50f63114a1be'
    def bitBucketStatusNotifierCredential = 'bitbucket_status_notifier'

    // After checkout will be replaced with ArtifactId
    def projectName = scm.getUserRemoteConfigs()[0].getUrl().tokenize('/').last().split("\\.")[0]
    // filter to get a file to archive.
    def artifactFilter = env["ARTIFACT_FILTER"] ?: '**/target/rpm/**/RPMS/noarch/*.rpm'
    def currentDirectory = pwd()

    //def gitBranch = env.GIT_BRANCH_NAME ?: 'master'

    def rpmSubDir = (buildBranch == 'master' || buildBranch.startsWith('rel')) ? "${projectName}" : "${projectName}-${buildBranch}"

    def rpmFilePath
    def rpmName
    def pulpRepoId = env["REPO_ID"] ?: 'mobi-snapshots'

    def artifactId
    def version

    properties([
            buildDiscarder(logRotator(artifactDaysToKeepStr: '', artifactNumToKeepStr: '', daysToKeepStr: '10', numToKeepStr: '10')),
            [$class: 'RebuildSettings', autoRebuild: false, rebuildDisabled: false],
            pipelineTriggers([])
    ])

    File file = new File("${env.WORKSPACE}/rpmexist")

    try {
        bitbucketStatusNotify ( buildState: 'INPROGRESS', credentialsId: "${bitBucketStatusNotifierCredential}" )

        currentBuildStage = 'Checkout Repository'
        stage("${currentBuildStage}") {
            git branch: env.BRANCH_NAME, credentialsId: "${gitCredential}", url: "${repoUrl}"
            artifactId = readMavenPom (file: 'pom.xml').getArtifactId()
            version = readMavenPom (file: 'pom.xml').getVersion()
        }

        currentBuildStage = 'Packaging'
        stage("${currentBuildStage}") {
            echo "Building project - ${artifactId} with Version - ${version}"
            sh 'mvn -s /home/build/.m2/settings.xml -Dmaven.repo.local=/home/build/.m2/repository -U -Prpm -Pgitversion clean deploy'
            echo 'Getting rpmName in file'
            dir ("target/rpm/${rpmSubDir}/RPMS/noarch/") {
                sh "ls | grep ${projectName} > rpmfilename"
                rpmName = readFile('rpmfilename')
            }
            rpmFilePath = "${currentDirectory}/target/rpm/${projectName}/RPMS/noarch/${rpmName}"
        }

        bitbucketStatusNotify ( buildState: 'SUCCESSFUL', credentialsId: "${bitBucketStatusNotifierCredential}" )

        if (isMasterOrReleaseBranch) {
            currentBuildStage = 'Update RC Version'
            stage("${currentBuildStage}") {
                def buildCount = env.BUILD_NUMBER
                echo 'Updating RC version in pom'
                sh "mvn build-helper:parse-version versions:set -DbuildNumber=\${buildCount} '-DnewVersion=\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.incrementalVersion}-RC\${buildNumber}' versions:commit"
            }
        } else {
            currentBuildStage = 'Update SNAPSHOT Version'
            stage("${currentBuildStage}") {
                echo 'Updating SNAPSHOT version in pom'
                sh "mvn build-helper:parse-version versions:set '-DnewVersion=\${parsedVersion.majorVersion}.\${parsedVersion.minorVersion}.\${parsedVersion.nextIncrementalVersion}-SNAPSHOT' versions:commit"
            }
        }

        currentBuildStage = 'Uploading RPM to mobi-snapshot repo'
        stage("${currentBuildStage}") {
            echo 'login to pulp repo'
            sh "pulp-admin login -u admin -p admin"
            //checking the rpm
            echo "checking the rpm in ${pulpRepoId} repo "
            sh "pulp-admin rpm repo content rpm --repo-id=${pulpRepoId} --match filename=${rpmName} > ${WORKSPACE}/rpmexist"
            if (file.length() == 0) {
                echo "uploading rpm "
                sh "pulp-admin rpm repo uploads rpm --repo-id ${pulpRepoId} -f ${rpmFilePath}"
                echo 'publishing rpm'
                sh "pulp-admin rpm repo publish run --repo-id ${pulpRepoId}"
                echo "Successfully logout from pulp repo"
                sh 'pulp-admin logout'
            } else {
                // RPM exist IN mobi-snapshot repo
                echo "${rpmName} exist in ${pulpRepoId} repo.No need to upload into pulp repo"
                sh "pulp-admin logout"
                error("Build failed because ${rpmName} exist in ${pulpRepoId}")
            }
        }

        currentBuildStage = 'Archive and publishing Junit results'
        stage("${currentBuildStage}") {
            echo 'archiving'
            archive "target/**/*"
            step([$class: 'JUnitResultArchiver', testResults: '**/target/surefire-reports/TEST-*Test.xml'])
        }

        currentBuildStage = 'Publishing Code coverage on jenkins'
        stage("${currentBuildStage}") {
            step( [ $class: 'JacocoPublisher' ] )
        }

        currentBuildStage = 'Sending Results to Sonarqube'
        stage("${currentBuildStage}") {
            sh 'mvn -s /home/build/.m2/settings.xml -Dmaven.repo.local=/home/build/.m2/repository sonar:sonar'
        }

        currentBuildStage = 'Update BitBucket Build Status'

        if (buildBranch == 'master' || buildBranch.startsWith('rel')) {
            currentBuildStage = 'Deploying to CI-CP'
            stage("${currentBuildStage}") {
                echo 'installing ansible'
                sh 'sudo yum install ansible -y'
                echo 'installing ansible config and pcc'
                sh 'sudo yum install mobi-sre-ansible-config-ci mobi-sre-ansible-pcc-ci -y'
                sh 'sudo cp /etc/ansible/playbooks/ci/ansible.cfg /etc/ansible/ansible.cfg'
                sh 'sudo ansible-playbook -vv /etc/ansible/playbooks/ci/msliv.yaml'
            }

            /*currentBuildStage = 'Run Automation Against CI'
            stage("${currentBuildStage}") {
                echo 'Checkout automation script'
                git branch: env.BRANCH_NAME, credentialsId: "${gitCredential}", url: "${repoContainer}:${repoOwner}/${automationRepoName}"
                echo 'Executing Automation Suite..'
                //sh 'mvn clean integration-test -Dtest_env=ci -Dit.test=com.mobitv.qa.test.matcher.PoisMatcherServiceRestTest'
                echo 'Automation Suite executed successfully!'
            }*/
        }
        sendEmail()
    } catch (any) {
        bitbucketStatusNotify ( buildState: 'FAILED', credentialsId: "${bitBucketStatusNotifierCredential}" )
        //Cleaning up workspace
        deleteDir()
        // send email
        sendEmail()
        throw any
    } finally {
        deleteDir()    //always cleanup workspace
    }
}

def sendEmail() {
    def ccEmailId = 'yogendras@cybage.com'
    def recipients = 'yogendras@cybage.com'
    def subject
    def message
    try {
        switch (currentBuild.currentResult) {
            case 'SUCCESS':
                if ('SUCCESS' != currentBuild.getPreviousBuild().getResult()) {
                    subject = "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Back to normal"
                    message = "Build back to normal: ${env.BUILD_URL}."
                    emailext(to: "${recipients}", replyTo: '$DEFAULT_REPLYTO', cc: '${ccEmailId}',
                            attachLog: true, sendToIndividuals: true,
                            subject: "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Back to normal", body: "Build back to normal: ${env.BUILD_URL}."
                    );
                } else {
                    subject = "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Success"
                    message = "Build success: ${env.BUILD_URL}."
                    emailext(to: "${recipients}", replyTo: '$DEFAULT_REPLYTO', cc: '${ccEmailId}',
                            attachLog: true, sendToIndividuals: true,
                            subject: "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Back to normal", body: "Build back to normal: ${env.BUILD_URL}."
                    );
                }
                break
            case 'UNSTABLE':
                subject = "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Unstable"
                message = "Build unstable: ${env.BUILD_URL}."
                break
            case 'FAILURE':
                subject = "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Failed"
                message = "Build failed: ${env.BUILD_URL}."
                break
            default:
                subject = "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - ${currentBuild.result}"
                message = "Build ${currentBuild.result}: ${env.BUILD_URL}."
                break
        }
        emailext(to: "${recipients}", replyTo: '$DEFAULT_REPLYTO', cc: '${ccEmailId}',
                attachLog: true, sendToIndividuals: true,
                subject: "${subject}", body: "${message}"
        );

    } catch (e) {
        emailext(to: "${recipients}", replyTo: '$DEFAULT_REPLYTO', cc: '${ccEmailId}',
                attachLog: true, sendToIndividuals: true,
                subject: "${env.JOB_NAME}' (${env.BUILD_NUMBER}) - Failure!", body: "Build failed ${env.BUILD_URL}.");
    }
}